<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <p><h3>Sign Up Form</h3></p>
    <form action="/welcome" method="POST">
    @csrf
        <label for="firstname">First name:</label><br><br>
        <input type="text" placeholder="First name" name="firstname" id="firstname"><br><br>

        <label for="lastname">Last name:</label><br><br>
        <input type="text" placeholder="Last name" name="lastname" id="lastname"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender" >Male <br>
        <input type="radio" name="gender" >Female <br>
        <input type="radio" name="gender" >Other <br><br>

        <label>Nationality:</label><br><br>
        <select name="neagara" id="">
            <option value="brunei">Brunei Darusallam</option>
            <option value="Filipina">Filipina</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Kamboja">Kamboja</option>
            <option value="Laos">Laos</option>
            <option value="Malasya">Malasya</option>
            <option value="Myanmar">Myanmar</option>
            <option value="Singapura">Singapura</option>
            <option value="Thailand">Thailand</option>
        </select><br><br>

        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="bahasaindonesia" >Bahasa Indonesia <br>
        <input type="checkbox" name="english">English <br>
        <input type="checkbox" name="Other" >Other <br><br>

        <label >Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up" name="submit">

    </form>
</body>
</html>